#!/usr/bin/python3

# Import ZeroMQ
import zmq

# Basic includes
import time

port = 12800

def socket_process(socket):
    while True:
        # Say something :)
        msg = "Hello, world!"
        socket.send(msg.encode('ascii'))

        # Now wait for reply
        ans = socket.recv()
        print("answer: ", ans)
        # Temporize a little bit to avoid overload
        time.sleep(5)

def main():
    # Initialize ZeroMQ context
    zeroMQContext = zmq.Context()

    # Make a socket
    socket = zeroMQContext.socket(zmq.REQ)

    # Connect the socket
    socket.connect("tcp://localhost:%s" % port)

    # The main loop of the server is inside this function
    socket_process(socket)

if __name__ == '__main__':
    main()
