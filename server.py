#!/usr/bin/python3

# Import ZeroMQ
import zmq

# Basic includes
import time

port = 12800

def socket_process(socket):
    while True:
        # Wait for a request from client
        msg = socket.recv()
        print("req: ", msg)
        # Temporize a little bit to avoid overload
        time.sleep(1)
        ans = "Hey! You said %s" % msg
        socket.send(ans.encode('ascii'))

def main():
    # Initialize ZeroMQ context
    zeroMQContext = zmq.Context()

    # Make a socket
    socket = zeroMQContext.socket(zmq.REP)

    # Bind the socket to the network stack
    socket.bind("tcp://*:%s" % port)

    # The main loop of the server is inside this function
    socket_process(socket)

if __name__ == '__main__':
    main()
